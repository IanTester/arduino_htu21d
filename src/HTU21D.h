/*
HTU21D/src/HTU21D.h - common classes
Copyright (C) 2020 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include <stdint.h>

#define HTU21D_ADDRESS 0x40

class HTU21D_reading;

class HTU21D {
private:
  enum class Command : uint8_t {
    // Hold the SCK line low while measuring
    TriggerTemperatureMeasurement_Hold	= 0xe3,
    TriggerHumidityMeasurement_Hold	= 0xe5,

    WriteUserRegister			= 0xe6,
    ReadUserRegister			= 0xe7,

    TriggerTemperatureMeasurement	= 0xf3,
    TriggerHumidityMeasurement		= 0xf5,

    SoftReset				= 0xfe,
  };

  enum class UserReg_mask : uint8_t {
    MeasurementResolution		= 0x81,
    EndOfBattery			= 0x40,
    // Bits 3, 4, and 5 are reserved
    EnableHeater			= 0x04,
    DisableOTP				= 0x02,
  };

  

  enum class UserReg_MeasurementResolution : uint8_t {
    RH12_T14				= 0x00,
    RH8_T12				= 0x01,
    RH10_T13				= 0x80,
    RH11_T11				= 0x81,
  };

  enum class LastMeasurement : uint8_t {
    None,
    Temperature,
    Humidity,
  };

  // Begins a transmission - caller must end it!
  void _send_command(Command cmd);

  uint16_t _raw_t, _raw_h;
  bool _error;
  uint8_t _userreg, _status;
  LastMeasurement _last_measurement;

  uint8_t _read_user_reg(void);
  void _write_user_reg(uint8_t val);

public:
  HTU21D() {}

  void begin(void);

  bool check(void);

  unsigned long measureTemperature(void);
  unsigned long measureHumidity(void);

  unsigned long reset(void);

  HTU21D_reading* readMeasurement(HTU21D_reading* reading = nullptr);

}; // class HTU21D


class HTU21D_reading {
private:
  uint16_t _raw_t, _raw_h;
  bool _have_t, _have_h;

  void _set_raw_t(uint16_t rt);
  void _set_raw_h(uint16_t rh);

  friend class HTU21D;

public:
  HTU21D_reading();

  bool haveTemperature(void) const;
  bool haveHumidity(void) const;

  float temperature(void) const;
  float humidity(void) const;
  float compensated_humidity(void) const;

}; // class HTU21D_reading
