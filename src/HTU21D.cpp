/*
HTU21D/src/HTU21D.cpp - common classes
Copyright (C) 2020 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "HTU21D.h"

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include <Wire.h>

void HTU21D::_send_command(Command cmd) {
  Wire.beginTransmission(HTU21D_ADDRESS);
  Wire.write(static_cast<uint8_t>(cmd));
}

uint8_t HTU21D::_read_user_reg(void) {
  _send_command(Command::ReadUserRegister);
  Wire.endTransmission();

  _error = false;
  if (Wire.requestFrom(HTU21D_ADDRESS, 1) < 1) {
    _error = true;
    return 0;
  }

  int val = Wire.read();
  if (val == -1) {
    _error = true;
    return 0;
  }

  return val;
}

void HTU21D::_write_user_reg(uint8_t val) {
  _send_command(Command::WriteUserRegister);
  Wire.write(val);
  Wire.endTransmission();

  _userreg = val;
}

void HTU21D::begin(void) {
  _error = false;
  _last_measurement = LastMeasurement::None;
}

bool HTU21D::check(void) {
  _read_user_reg();
  return !_error;
}

unsigned long HTU21D::measureTemperature(void) {
  _send_command(Command::TriggerTemperatureMeasurement);
  Wire.endTransmission();
  _last_measurement = LastMeasurement::Temperature;

  switch (_userreg & static_cast<uint8_t>(UserReg_mask::MeasurementResolution)) {
  case static_cast<uint8_t>(UserReg_MeasurementResolution::RH11_T11):
    return 7000;
  case static_cast<uint8_t>(UserReg_MeasurementResolution::RH8_T12):
    return 13000;
  case static_cast<uint8_t>(UserReg_MeasurementResolution::RH10_T13):
    return 25000;
  default:
    break;
  }
  return 50000; // 50 ms for 14 bits
}

unsigned long HTU21D::measureHumidity(void) {
  _send_command(Command::TriggerHumidityMeasurement);
  Wire.endTransmission();
  _last_measurement = LastMeasurement::Humidity;


  switch (_userreg & static_cast<uint8_t>(UserReg_mask::MeasurementResolution)) {
  case static_cast<uint8_t>(UserReg_MeasurementResolution::RH8_T12):
    return 3000;
  case static_cast<uint8_t>(UserReg_MeasurementResolution::RH10_T13):
    return 5000;
  case static_cast<uint8_t>(UserReg_MeasurementResolution::RH11_T11):
    return 8000;
  default:
    break;
  }
  return 16000; // 16 ms for 12 bits
}

unsigned long HTU21D::reset(void) {
  _send_command(Command::SoftReset);
  Wire.endTransmission();
  _last_measurement = LastMeasurement::None;

  return 15000; // 15 ms
}

/* Originally copied from SparkFun's slightly more complete library:
   https://github.com/sparkfun/HTU21D_Breakout/blob/master/Libraries/Arduino/src/SparkFunHTU21D.cpp
*/
uint8_t calcCRC(uint16_t data, uint8_t crc) {
  uint32_t remainder = static_cast<uint32_t>(data) << 8;        // Pad with 8 bits because we have to add in the check value
  remainder |= crc;                             // Add on the check value

  uint32_t divisor = 0x988000L;                 // This is the polynomial shifted to farthest left of three bytes

  for (int i = 0 ; i < 16 ; i++) {              // Operate on only 16 positions of max 24. The remaining 8 are our remainder and should be zero when we're done.
    if (remainder & 1L << (23 - i))             // Check if there is a one in the left position
      remainder ^= divisor;

    divisor >>= 1;                              // Rotate the divisor max 16 times so that we have 8 bits left of a remainder
  }

  return remainder & 0xff;
}


HTU21D_reading* HTU21D::readMeasurement(HTU21D_reading* reading) {
  if (_last_measurement == LastMeasurement::None) {
    _error = true;
    return nullptr;
  }

  if (Wire.requestFrom(HTU21D_ADDRESS, 3) < 3) {
    _error = true;
    return nullptr;
  }

  while (!Wire.available()) {}

  _error = false;

  int high = Wire.read();
  if (high == -1) {
    _error = true;
    return nullptr;
  }

  int low = Wire.read();
  if (low == -1) {
    _error = true;
    return nullptr;
  }
  _status = low & 0x03;

  int checksum = Wire.read();
  if (checksum == -1) {
    _error = true;
    return nullptr;
  }

  // TODO: Figure out what to do with/about the status bits
  /*
  if (_status != 0) {
    _error = true;
    return nullptr;
  }
  */

  uint16_t raw = (static_cast<uint16_t>(high & 0xff) << 8) | (low & 0xff);
  uint8_t crc = calcCRC(raw, checksum);
  raw &= 0xfffc;                // remove the two LSB status bits
  if (crc != 0) {
    _error = true;
    return nullptr;
  }

  if (reading == nullptr)
    reading = new HTU21D_reading();
  switch (_last_measurement) {
  case LastMeasurement::Temperature:
    reading->_set_raw_t(raw);
    break;
  case LastMeasurement::Humidity:
    reading->_set_raw_h(raw);
    break;
  }

  return reading;
}

HTU21D_reading::HTU21D_reading() :
  _have_t(false),
  _have_h(false)
{}

void HTU21D_reading::_set_raw_t(uint16_t rt) {
  _raw_t = rt;
  _have_t = true;
}

void HTU21D_reading::_set_raw_h(uint16_t rh) {
  _raw_h = rh;
  _have_h = true;
}

bool HTU21D_reading::haveTemperature(void) const {
  return _have_t;
}

bool HTU21D_reading::haveHumidity(void) const {
  return _have_h;
}

float HTU21D_reading::temperature(void) const {
  if (!_have_t)
    return -100.0;

  return (_raw_t * 0.0026812744140625) - 46.85;
}

float HTU21D_reading::humidity(void) const {
  if (!_have_h)
    return -100.0;

  return (_raw_h * 0.0019073486328125) - 6.0;
}

float HTU21D_reading::compensated_humidity(void) const {
  if ((!_have_t) || (!_have_h))
    return -100.0;

  return humidity() - ((25.0 - temperature()) * 0.15);
}

